arch := $(shell uname -m)
tag = booniepepper/fvm:latest



### PyPI

.PHONY: build
build: log
	command -v flit >/dev/null || pipx install flit
	flit build >./log/flit-build.log

.PHONY: test
test: build
	flit install >./log/flit-build.log # TODO: Run it

.PHONY: release
release: build test
	flit publish



### DOCKER

.PHONY: docker
docker: log
	cp ./fvm/run.py ./docker/fvm.py
	cd ./docker/ \
	&& docker buildx build --no-cache -t=$(tag) . >../log/docker-build.log \
	&& docker run $(tag) eval-fmv-bitcode '1<1[<[0<]100<]<[0<]'
	rm ./docker/fvm.py

.PHONY: docker-release
docker-release: docker
	docker push $(tag)



### MISC

log:
	mkdir -p ./log
